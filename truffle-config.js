const HDWalletProvider = require('truffle-hdwallet-provider');
const { existsSync } = require('fs');

const configFilePath = './config.js'
let config
if (existsSync(configFilePath)) {
  config = require(configFilePath)
} else {
  console.warn(`\nWARNING: config.js not setup - infura networks disabled\n`)
}

module.exports = {
  compilers: {
    solc: {
      version: "0.5.9+commit.e560f70d",
      docker: false,
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        },
      }
    }
  },
  /**
   * Networks define how you connect to your ethereum client and let you set the
   * defaults web3 uses to send transactions. If you don't specify one truffle
   * will spin up a development blockchain for you on port 9545 when you
   * run `develop` or `test`. You can ask a truffle command to use a specific
   * network from the command line, e.g
   *
   * $ truffle test --network <network-name>
   */
  networks: {
    ropsten: {
      provider: function() {
        return new HDWalletProvider(config.mnemonics.ropsten, "https://ropsten.infura.io/v3/"+config.apikey)
      },
      network_id: 3,
      gas: 4500000,
      gasPrice: 10000000000
    },
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*' // eslint-disable-line camelcase
    },
    test: {
      host: 'localhost',
      port: 8545,
      network_id: '*' // eslint-disable-line camelcase
    },
    ganache: {
      host: 'localhost',
      port: 8545,
      network_id: '*' // eslint-disable-line camelcase
    }
  }
}
