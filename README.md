# Cross Chain Atomic Swap BTC-ETH

## abstract
Atomic swaps are practical for exchanging different cryptocurrencies in avoiding any trusted third-parties. This project shows a swap between Bitcoin and Ethereum blockchain using payment channels tools such as Hashed Timelock Contracts . When the protocol is followed by both participants, it guarantees that the swap will either go ahead without any risk or be aborted. Furthermore, there is no scenario where someone can control both parties coins, therefore the entire swap process is non-custodial in nature.

**Keyword**:
`Blockchain, Bitcoin, Ethereum, Atomic Swap`

## Contract address Deployed
* Ropsten : 0x76496a39Ab2AfeBFe14376A15d4edC808688Cea3


## Prerequisites

* NodeJS version 10.16.0 or higher
* Ganache version 2.0.1

## Installation

Go into the  root folder `./` and install the dependencies for smart contract part.

```
npm install
```

Then go into the app folder `./app` and install dependencies for the application.

```
npm install
```


### Deployment of the smart contract via truffle

First we need to build and deploy the smart contract in ethereum.



1. Launch Ganache
2. Go into the the folder `./`
3. Build the contract : `npm run build`

Now we have the choice, if we want to use ganache or Testnet like etherscan. **WARNING**, ganache must be **opened** for deploying on ganache.

To deploy with ganache :

```
truffle deploy --network ganache
```

or

To Deploy with ropsten :
```
truffle deploy --network ropsten
```

Now our contract are deployed on the ethereum blockchain, we can open the web application. In this project there are 2 implementations, `dev mode` for the development and `prod mode` for the production

### Dev mode

To use the project in development mode go into the development folder or branch.

1. Go into the folder : `./app`
2. Build the project : `npm run build:dev`
3. Launch the application : `npm run dev`
4. Host the html site on port 8080
5. In the browser, GOTO `http://localhost:8080`

### Prod mode

To use the project in production mode go into the prod folder or branch.

1. Go into the folder : `./app`
2. Build the project : `npm run build:prod`
3. Launch the application : `npm run start`
4. Host the html site on port 8080
5. In the browser, GOTO `http://localhost:8080`


# End-To-end Test Case

## Alice initiates Swap on bitcoin 

1. Alice generate the secret and hash it
2. Put the amount an timelock
3. Click on initiate Swap

## Bob initiates Swap on bitcoin
 4. Get the hash by Alice
 5. Fill the amount and the timelock
 6. Click on initiate Swap

## Alice Claim Swap on Ethereum
7. Alice fills the preimage at Ethereum locking script
8. Click on claim

## Bob Claim Swap on Ethereum
7. Bob fills the preimage with secret get from the blockchain
8. Click on claim

## Bob Claim Swap on Ethereum
7. Bob fills the preimage with secret get from the blockchain
8. Click on claim