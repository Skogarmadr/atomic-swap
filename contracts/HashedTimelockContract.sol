pragma solidity 0.5.9;


/**
@title Hashed Timelock Contract (HTLC)
@author Luca Srdjenovic <luca.srdjenovi@gmail.com>

Cross chain Atomic-Swap

*/
contract HashedTimelockContract {

    event NewLockContract(
        address indexed sender,
        address indexed receiver,
        uint amount,
        bytes32 hashlock,
        uint timelock
    );

    event FundClaimed(
        address indexed receiver,
        bytes32 preimage,
        uint amount
    );

    event RefundSpent(address indexed owner, uint amount);

    struct LockContract {
        address payable sender;
        address payable receiver;
        uint amount; // in wei
        bytes32 hashlock; // sha256 hash
        uint timelock; // block number - locked UNTIL this time
    }

    modifier busy(address _address){
        require(busyAddresses[_address] == true, "There is no contract.");
        _;
    }

    modifier noBusy(){
        require(busyAddresses[msg.sender] == false, "There is already a contract.");
        _;
    }

    modifier onlySender() {
        require(msg.sender == ownerToContract[msg.sender].sender, "You are not the sender of the contract");
        _;
    }

    modifier onlyReceiver(address _ownerAddress){
        require(msg.sender == ownerToContract[_ownerAddress].receiver, "You are not the receiver of the contract");
        _;
    }

    modifier hashlockMatches(address _ownerAddress, bytes32 _preimage) {
        require(
            ownerToContract[_ownerAddress].hashlock == sha256(abi.encodePacked(_preimage)),
            "Claiming : The given preimage doesn't match."
        );
        _;
    }

    modifier claimable(address _ownerAddress){
        require(
            ownerToContract[_ownerAddress].timelock > block.number,
            "Claiming : The timelock has been reached, you can't claim"
        );
        _;
    }

    modifier refundable(){
        require(
            ownerToContract[msg.sender].timelock < block.number,
            "Refunding : The timelock hasn't been reached, you can't refund"
        );
        _;
    }

    modifier minimumFund() {
        require(msg.value > 0, "Funding : No amount given !");
        _;
    }

    modifier differentReceiver(address _receiverAddress) {
        require(
            msg.sender != _receiverAddress,
            "Funding : You can't send to yourself."
        );
        _;
    }

    mapping (address => LockContract) public ownerToContract;
    mapping (address => bool) public busyAddresses;
    /**
    @notice Lock `a new LockContract` from the account of
    `message.caller.address()` with the differents parameters.
    @dev Create a new LockContract and lock it.
    @param _receiver The address of the recipient of the Ethers.
    @param _hashlock The hashlock of the contract.
    @param _block The number to add for the timelock.
    */
    function lock (address payable _receiver, bytes32 _hashlock, uint _block)
        external
        payable
        noBusy
        minimumFund
        differentReceiver(_receiver)
    {
        ownerToContract[msg.sender] = LockContract(
            msg.sender,
            _receiver,
            msg.value,
            _hashlock,
            block.number + _block
        );

        busyAddresses[msg.sender] = true;

        emit NewLockContract(msg.sender, _receiver, msg.value, _hashlock, block.number + _block);
    }

    /**
    @notice For unlocking the contract, the caller must be the receiver and timelock < number.block
    @dev Unlock the contract with the address of the caller. This function allows to withdraw the balance of the contract by the receiver's address.
    @param _owner The address of the recipient of the balance.
    @param _preimage The preimage to unlock.
    @return true
    */
    function unlock(address _owner, bytes32 _preimage)
        external
        onlyReceiver(_owner)
        busy(_owner)
        hashlockMatches(_owner, _preimage)
        claimable(_owner)
        returns (bool)
    {
        LockContract memory c = ownerToContract[_owner];

        busyAddresses[_owner] = false;

        c.receiver.transfer(c.amount);

        emit FundClaimed(c.receiver, _preimage, c.amount);
        return true;
    }

    /**
    @notice For getting the refund, the function must be refundable, timelock > block.number
    @dev Refund the sender when the timelock is reached.
    @return true
    */
    function refund() external onlySender busy(msg.sender) refundable returns (bool) {
        LockContract memory c = ownerToContract[msg.sender];

        busyAddresses[msg.sender] = false;
        c.sender.transfer(c.amount);

        emit RefundSpent(c.sender, c.amount);

        return true;
    }

    function getBlockNumber() public view returns(uint) {
        return block.number;
    }

    function getBalance() public view returns (uint) {
        return address(this).balance;
    }
}
