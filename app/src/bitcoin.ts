import * as bitcoin from 'bitcoinjs-lib';

import { randomBytes } from 'crypto';
import $ from 'jquery';
import { alice, bob } from './bitcoin/wallets.json';

import config from './bitcoin/config.json';



const bip68 = require('bip68');
const APINETWORK = config.APINETWORK;
const NETWORK = bitcoin.networks.testnet;
const keyPairAlice = bitcoin.ECPair.fromWIF(alice[0].wif, NETWORK);
const p2wpkhAlice = bitcoin.payments.p2wpkh({ pubkey: keyPairAlice.publicKey, network: NETWORK });
const addressAlice = p2wpkhAlice.address as string;
const keyPairBob = bitcoin.ECPair.fromWIF(bob[0].wif, NETWORK);
const p2wpkhBob = bitcoin.payments.p2wpkh({ pubkey: keyPairBob.publicKey, network: NETWORK });
const addressBob = p2wpkhBob.address as string;
const hashType = bitcoin.Transaction.SIGHASH_ALL;


async function mainBtc() {
  startBtc();
  iniateLockFundBtc();
  initiateClaimFundBtc();
  initiateRefundFundBtc();
}


async function generateSecretAlice() {

  const secret = randomBytes(32);
  const secretHash = bitcoin.crypto.sha256(secret);

  $("#secretAlice").text(secret.toString('hex'));
  $("#secretHashAlice").text(secretHash.toString('hex'));

}

async function startBtc() {
  $("#btnSecretAlice").click(async () => {
    await generateSecretAlice();
  });
}

async function iniateLockFundBtc() {
  $("#btnInitiateAlice").click(() => {
    lockFund();
  });
}

async function initiateClaimFundBtc() {
  $("#btnClaimBtc").click(() => {
    claimFundBtc();
  });
}

async function initiateRefundFundBtc() {
  $("#btnRefundBtc").click(() => {
    refundFundBtc();
  });
}

function csvCheckSigOutput(senderPubkeyHash: bitcoin.StackElement, receiverPubkeyHash: bitcoin.StackElement, hash: bitcoin.StackElement, sequence: number) {
  return bitcoin.script.compile([
    bitcoin.opcodes.OP_IF, // claim for buyer
    bitcoin.opcodes.OP_SHA256,
    hash,
    bitcoin.opcodes.OP_EQUALVERIFY,
    bitcoin.opcodes.OP_DUP,
    bitcoin.opcodes.OP_HASH160,
    receiverPubkeyHash,
    bitcoin.opcodes.OP_ELSE, // Refund for seller, lock the transaction until the locktime has been reached
    bitcoin.script.number.encode(sequence), // check locktime
    bitcoin.opcodes.OP_CHECKSEQUENCEVERIFY, // it verifies, drop time of the stack
    bitcoin.opcodes.OP_DROP, // drop time
    bitcoin.opcodes.OP_DUP,
    bitcoin.opcodes.OP_HASH160,
    senderPubkeyHash,
    bitcoin.opcodes.OP_ENDIF,
    bitcoin.opcodes.OP_EQUALVERIFY,
    bitcoin.opcodes.OP_CHECKSIG,
  ]);
}


async function lockFund() {

  const revSecret = $('#secretAlice').text();
  const revHash = Buffer.from($('#secretHashAlice').text(), 'hex');
  const amount = parseFloat($('#amountAlice').val() + "");
  let timelimit = 2;

  if ($('#timelimitBtc').val()) {
    timelimit = parseFloat($('#timelimitBtc').val() + "");
  }

  const amountSatoshis = Math.floor(amount * 1e8);
  console.log(amountSatoshis);

  const pubKeyAliceHash = bitcoin.crypto.hash160(keyPairAlice.publicKey);
  console.log('Alice pubkey hash : ' + pubKeyAliceHash.toString('hex'));


  const pubKeyBobHash = bitcoin.crypto.hash160(keyPairBob.publicKey);
  console.log('Bob pubkey hash : ' + pubKeyBobHash.toString('hex'));

  // Encode the sequence value according to BIP68 specification (now + 2 blocks).
  const sequence = bip68.encode({ blocks: timelimit });
  console.log('sequence  ', sequence);

  const witnessScript = csvCheckSigOutput(pubKeyAliceHash, pubKeyBobHash, revHash, sequence);
  console.log('redeemScript  ', witnessScript.toString('hex'));

  const p2wsh = bitcoin.payments.p2wsh({ redeem: { output: witnessScript }, network: NETWORK });
  const p2wshAddress = p2wsh.address as string;
  console.log('P2SH address  ', p2wsh.address);

  const objectJSON = {
    "amount": amountSatoshis,
    "isBusy": true,
    "p2wshAddress": p2wshAddress,
    "pubKeyAliceHash": pubKeyAliceHash.toString('hex'),
    "pubKeyBobHash": pubKeyBobHash.toString('hex'),
    "revHash": $('#secretHashAlice').text(),
    "revSecret": revSecret,
    "timelimit": timelimit,
    "witnessScript": witnessScript.toString('hex')
  };

  localStorage.setItem('transactionAlice', JSON.stringify(objectJSON));

  try {
    await $.get('https://chain.so/api/v2/get_tx_unspent/' + APINETWORK + addressAlice).done((response) => {
      const utxos = response.data.txs;

      console.log(utxos);


      if (utxos.length === 0) {
        console.log("no utxos found in funding address : ", addressAlice);
      } else {
        $.get('https://chain.so/api/v2/get_tx_unspent/' + APINETWORK + p2wshAddress).done((resP2wsh) => {

          const utxosp2wsh = resP2wsh.data.txs;

          if (utxosp2wsh.length === 0) {
            const txb = new bitcoin.TransactionBuilder(NETWORK);

            let totalBalance = 0;
            let inputCount = 0;
            const outputCount = 2;
            const values = [utxos.length];
  
            for (const utxo in utxos) {
              if (utxo) {
                totalBalance += Math.floor(parseFloat(utxos[utxo].value) * 1e8);
                values[inputCount] = Math.floor(parseFloat(utxos[utxo].value) * 1e8);
                txb.addInput(utxos[utxo].txid, utxos[utxo].output_no, undefined, p2wpkhAlice.output);
                console.log("adding input: " + utxos[utxo].txid + " to funding transaction");
                inputCount++;
              }
  
            }
  
            console.log(`Balance of address : ${totalBalance} satoshis`);
  
            try {
              $.get('https://fees.truelevel.io/api/v1/btc/deals').done((res) => {
                const estimatedTxSize = inputCount * 104 + outputCount * 32;
                let index = 0;
  
                for (let i = 0; i < res.length; i++) {
                  if (res[i].targetBlock === 0.5) {
                    index = i;
                    break;
                  }
                }
  
                console.log(res[index]);
  
  
                const estimatedFeeAmount = Math.floor(res[index].feeRate * estimatedTxSize);
                console.log(res[index].feeRate);
  
  
  
                console.log('estimated_fee_amount : ' + estimatedFeeAmount);
  
                const sendAmount = amountSatoshis + estimatedFeeAmount;
  
                if (totalBalance - sendAmount > 0) {
                  if (totalBalance - sendAmount !== 0) {
                    txb.addOutput(addressAlice, (totalBalance - sendAmount));
                  }
  
                  txb.addOutput(p2wshAddress, sendAmount - estimatedFeeAmount);
  
                  for (let i = 0; i < inputCount; i++) {
                    txb.sign(i, keyPairAlice, undefined, undefined, values[i]);
                  }
  
                  const fundingTransaction = txb.buildIncomplete();
                  console.log("");
                  console.log("===== Funding Transaction to " + p2wshAddress + " ) =======");
                  console.log("Transaction ID: " + fundingTransaction.getId());
                  console.log("Transaction (needed for broadcasting TX): " + fundingTransaction.toHex());
                  // console.log("Funding tx size: "+funding_transaction.byteLength())
                  console.log("");
  
                  $('#redeemScriptAlice').text(witnessScript.toString('hex'));
                  $('#txIdInitiateAlice').text(fundingTransaction.getId());
  
                   broadcastTx(fundingTransaction.toHex());
  
                } else {
                  alert('not enought of satoshis');
                }
  
  
              });
            } catch (error) {
              console.log(error);
  
            }
          } else {
            alert("This contract is already funded !");
          }
          
        });

      }

    });

  } catch (error) {
    console.log(error);

  }
}

async function claimFundBtc() {
  const preimage = $('#secretClaimBtc').val() + "";


  const obj = JSON.parse(localStorage.getItem('transactionAlice') || '{}');
  if (obj.isBusy === true) {
    console.log(obj.witnessScript);


    const witnessScript = Buffer.from(obj.witnessScript, 'hex');
    const p2wsh = bitcoin.payments.p2wsh({ redeem: { output: witnessScript }, network: NETWORK });
    const p2wshAddress = p2wsh.address as string;

    try {
      await $.get('https://chain.so/api/v2/get_tx_unspent/' + APINETWORK + p2wshAddress).done((response) => {
        const utxos = response.data.txs;

        if (utxos.length === 0) {
          console.log("no utxos found in funding address : ", p2wshAddress);
        } else {
          const txb = new bitcoin.TransactionBuilder(NETWORK);
          txb.setVersion(2);

          let totalBalance = 0;
          let inputCount = 0;

          for (const utxo in utxos) {
            if (utxo) {
              totalBalance += Math.floor(parseFloat(utxos[utxo].value) * 1e8);
              txb.addInput(utxos[utxo].txid, utxos[utxo].output_no, 0, p2wsh.output);
              console.log("adding input: " + utxos[utxo].txid + " to claim transaction");
              inputCount++;
            }
          }

          console.log(`Balance of address : ${totalBalance} satoshis`);


          try {
            $.get('https://fees.truelevel.io/api/v1/btc/deals').done((res) => {

              const estimatedTxSize = 104 + 32;
              let index = 0;

              for (let i = 0; i < res.length; i++) {
                if (res[i].targetBlock === 0.5) {
                  index = i;
                  break;
                }
              }

              console.log(res[index]);
              const estimatedFeeAmount = Math.floor(res[index].feeRate * estimatedTxSize);
              console.log(res[index].feeRate);
              const sendAmount = totalBalance - estimatedFeeAmount;

              console.log(estimatedFeeAmount);
              console.log(sendAmount);

              txb.addOutput(addressBob, sendAmount);

              const txRaw = txb.buildIncomplete();

              const signatureHash = txRaw.hashForWitnessV0(0, p2wsh.redeem!.output!, totalBalance, hashType);

              const witnessStack = bitcoin.payments.p2wsh({
                redeem: {
                  input: bitcoin.script.compile([
                    bitcoin.script.signature.encode(keyPairBob.sign(signatureHash), hashType),
                    keyPairBob.publicKey,
                    Buffer.from(preimage, 'hex'),
                    bitcoin.opcodes.OP_TRUE
                  ]),
                  output: witnessScript
                },
              }).witness;

              if (witnessStack) {
                txRaw.setWitness(0, witnessStack);
                console.log("");
                console.log("===== Claim TX from " + p2wshAddress + " to Bob address =======");
                console.log("attempting to broadcast transaction:");
                console.log("Transaction ID: " + txRaw.getId());
                console.log("Transaction: " + txRaw.toHex());
                console.log(preimage);

                broadcastTx(txRaw.toHex());

                $('#txIdClaimBtc').text(txRaw.getId());
              } else {
                console.log("Redeem script undefined");

              }


            });
          } catch (error) {
            console.log(error);

          }
        }
      });
    } catch (error) {
      console.log(error);

    }

  } else {
    alert("You have not fund a htlc");
  }


}

async function refundFundBtc() {
  const obj = JSON.parse(localStorage.getItem('transactionAlice') || '{}');
  if (obj.isBusy === true) {


    const witnessScript = Buffer.from(obj.witnessScript, 'hex');
    const sequence = obj.timelimit;
    console.log(sequence);

    const p2wsh = bitcoin.payments.p2wsh({ redeem: { output: witnessScript }, network: NETWORK });
    const p2wshAddress = p2wsh.address;

    try {
      await $.get('https://chain.so/api/v2/get_tx_unspent/' + APINETWORK + p2wshAddress).done((response) => {
        const utxos = response.data.txs;

        console.log(utxos);
        if (utxos.length === 0) {
          console.log("no utxos found in funding address : ", p2wshAddress);
        } else {
          const txb = new bitcoin.TransactionBuilder(NETWORK);
          txb.setVersion(2);

          let totalBalance = 0;
          let inputCount = 0;

          for (const utxo in utxos) {
            if (utxo && inputCount < 1) {
              totalBalance += Math.floor(parseFloat(utxos[utxo].value) * 1e8);
              txb.addInput(utxos[utxo].txid, utxos[utxo].output_no, sequence, p2wsh.output);
              console.log("adding input: " + utxos[utxo].txid + " to claim transaction");
              inputCount++;
            }
          }

          console.log(`Balance of address : ${totalBalance} satoshis`);
          try {
            $.get('https://fees.truelevel.io/api/v1/btc/deals').done((res) => {

              const estimatedTxSize = 104 + 32;
              let index = 0;

              for (let i = 0; i < res.length; i++) {
                if (res[i].targetBlock === 0.5) {
                  index = i;
                  break;
                }
              }

              console.log(res[index]);
              const estimatedFeeAmount = Math.floor(res[index].feeRate * estimatedTxSize);
              console.log(res[index].feeRate);
              const sendAmount = totalBalance - estimatedFeeAmount;

              console.log(estimatedFeeAmount);
              console.log(sendAmount);

              txb.addOutput(addressAlice, sendAmount);

              const txRaw = txb.buildIncomplete();

              if (p2wsh.redeem) {
                if (p2wsh.redeem.output) {
                  const signatureHash = txRaw.hashForWitnessV0(0, p2wsh.redeem.output, totalBalance, hashType);

                  const witnessStack = bitcoin.payments.p2wsh({
                    redeem: {
                      input: bitcoin.script.compile([
                        bitcoin.script.signature.encode(keyPairAlice.sign(signatureHash), hashType),
                        keyPairAlice.publicKey,
                        bitcoin.opcodes.OP_FALSE
                      ]),
                      output: witnessScript
                    },
                  }).witness;

                  if (witnessStack) {
                    txRaw.setWitness(0, witnessStack);
                    console.log("");
                    console.log("===== Claim TX from " + p2wshAddress + " to Bob address =======");
                    console.log("attempting to broadcast transaction:");
                    console.log("Transaction ID: " + txRaw.getId());
                    console.log("Transaction: " + txRaw.toHex());

                    broadcastTx(txRaw.toHex());

                    $('#txIdRefundBtc').text(txRaw.getId());
                  }
                }

              } else {
                console.log("UNDEFINED");

              }
            });
          } catch (error) {
            console.log(error);

          }
        }
      });
    } catch (error) {
      console.log(error);

    }

  } else {
    alert("You have not fund a htlc");
  }


}

async function broadcastTx(txHex: string) {

  try {
    $.post('https://chain.so/api/v2/send_tx/' + APINETWORK, "tx_hex=" + txHex).done(() => {
      alert("Tx broadcasted !!! ");
    });
  } catch (error) {
    console.log(error);
  }

}

async function getBalance(address: string) {
  try {
    $.get('https://chain.so/api/v2/get_address_balance/' + APINETWORK + address).done((response) => {
      alert(response.data.confirmed_balance);
    });
  } catch (error) {
    console.log(error);
  }
}



async function writeTransactionFile() {
  const revSecret = $('#secretAlice').html();
  const revHash = $('#secretHashAlice').text();
  const amount = $('#amountAlice').val();
  const timelimit = $('#timelimitBtc').val();


  const objectJSON = {
    "amount": amount,
    "flag": 1,
    "revHash": revHash,
    "revSecret": revSecret,
    "timelimit": timelimit
  };

  console.log(objectJSON);


  localStorage.setItem('transactionAlice', JSON.stringify(objectJSON));


  const obj = localStorage.getItem('transactionAlice');
  console.log(obj);


}


mainBtc().catch();