import $ from 'jquery';
import { alice, bob } from './bitcoin/wallets.json';

declare let window: any; // <--- Declare it like this

async function main() {

    await $(document).ready(() => {
      if(window.ethereum)
      {
        window.ethereum.enable();
        console.log(localStorage.getItem('transactionAlice'));
      
        $("#amountAlice").val("0.001");
        $("#btcAliceAddress").val(alice[0].p2wpkh);
        $("#btcBobAddress").val(bob[0].p2wpkh);
    
        $("#ethAliceAddress").val('0x26020cEFa4CCEF183C258D096b38C1b05296623d'.toLowerCase());
        $("#ethBobAddress").val('0x3686ea559327D37D64db0c46c404dbE6208Ab936'.toLowerCase());
    
        $('#btcAliceValue').on('input', (e) => {
          convertBtcToEth($('#btcAliceValue').val());
        });
    
        $('#ethBobValue').on('input', (e) => {
          convertEthToEth($('#ethBobValue').val());
        });
      } else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
      }
    });
}

async function convertBtcToEth(value: any) {
  try {
    $.get('https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=ETH').done((data) => {
      $('#ethAliceValue').val((value * data[0].price_eth).toFixed(6) + "");
    });
  } catch (error) {
    console.log(error);
  }
}

async function convertEthToEth(value: any) {
  try {
    $.get('https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=BTC').done((data) => {
      $('#btcBobValue').val((value * data[0].price_btc).toFixed(6) + "");
    });
  } catch (error) {
    console.log(error);
  }
}

main().catch(console.error);
