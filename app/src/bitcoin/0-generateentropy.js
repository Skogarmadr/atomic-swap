/** 
  * 1-generatewallet.js
  * 
  * @fileOverview Create and store an entropy for Alice and Bob.
  * @author Luca Srdjenovic luca.srdjenovic@he-arc.ch
  * @version 1.0.0
*/

const crypto = require('crypto');
const configFileName = './config.json';

var argv = require('yargs')
    .usage('Usage: $0')
    .help()
    .argv;

if (argv.config) {
   console.log("Using specified config file: "+argv.config);
   var config = require('./'+argv.config);
} else {
   console.log("Using default config (config.json)");
   var config = require(configFileName);
}



// 128 bit entropy => 12 words mnemonics
// Generate random entropy
const alice_randomBytes = crypto.randomBytes(16) // 128 bits is enough
const bob_randomBytes = crypto.randomBytes(16);

const alice_entropy = alice_randomBytes.toString('hex');
const bob_entropy = bob_randomBytes.toString('hex');

console.log("==============ENTROPY=======================");
console.log("===Alice's entropy :", alice_entropy);
console.log("===Bob's entropy :", bob_entropy);

/**
 * Write data into config.json file
 */
config.alice.entropy = alice_entropy;
config.bob.entropy = bob_entropy;

var fs = require("fs");

fs.writeFile(configFileName, JSON.stringify(config, null, 4), function (err) {
  if (err) return console.log(err);
  console.log(JSON.stringify(config));
  console.log('writing to ' + configFileName);
});

