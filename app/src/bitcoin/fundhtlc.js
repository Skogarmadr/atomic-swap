/** 
  * 2-fundingaddress.js
  * 
  * @fileOverview Generate P2SH address
  * @author Luca Srdjenovic luca.srdjenovic@he-arc.ch
  * @version 1.0.0
*/


const bip65 = require('bip65'); //Timelock encoding
const bitcoin = require('bitcoinjs-lib');



var argv = require('yargs')
    .usage('Usage: $0')
    .help()
    .argv;

if (argv.config) {
   console.log("Using specified config file: "+argv.config);
   var config = require('./'+argv.config);
} else {
   console.log("Using default config (config.json)");
   var config = require("./config");
}

if(config.use_regtest){
  var NETWORK = bitcoin.networks.regtest;
}else {
  if (config.use_testnet) {
      var NETWORK = bitcoin.networks.testnet;
  } else {
      var NETWORK = bitcoin.networks.bitcoin;
  }
}

const hashType = bitcoin.Transaction.SIGHASH_ALL



function csvCheckSigOutput(senderPubKey, receiverPubKey, hash, sequence) {
  return bitcoin.script.compile([
    
    bitcoin.opcodes.OP_IF, //claim for buyer
        bitcoin.opcodes.OP_SHA256,
        hash,
        bitcoin.opcodes.OP_EQUALVERIFY,
        receiverPubKey,
        bitcoin.opcodes.OP_CHECKSIG,
    bitcoin.opcodes.OP_ELSE, // Refund for seller, lock the transaction until the locktime has been reached
        bitcoin.script.number.encode(sequence), //check locktime
        bitcoin.opcodes.OP_CHECKSEQUENCEVERIFY, // it verifies, drop time of the stack
        bitcoin.opcodes.OP_DROP, //drop time
        senderPubKey,
        bitcoin.opcodes.OP_CHECKSIG,
    bitcoin.opcodes.OP_ENDIF,
    //bitcoin.opcodes.OP_CHECKSIG,
  ])
};


const secret = "test"
console.log(secret);

const secretHash = bitcoin.crypto.sha256(secret);
console.log('hash : ', secretHash.toString('hex'));

const keyPairAlice0 = bitcoin.ECPair.fromWIF(alice[0].wif, NETWORK)
const p2pkh =bitcoin.payments.p2pkh({pubkey: keyPairAlice0.publicKey,  network:NETWORK})
const p2wpkhAlice0 = bitcoin.payments.p2wpkh({pubkey: keyPairAlice0.publicKey,  network:NETWORK})

const keyPairBob0 = bitcoin.ECPair.fromWIF(bob[0].wif, NETWORK)
const bobAddress = bitcoin.payments.p2wpkh({pubkey: keyPairBob0.publicKey,  network:NETWORK})

const sequence = bip65.encode({blocks: 5}) //6 hours ago
console.log('sequence  ', sequence)

//const redeemScript = csvCheckSigOutput(keyPairAlice0, keyPairBob0, secretHash, sequence)
const redeemScript = csvCheckSigOutput(keyPairAlice0.publicKey, keyPairBob0.publicKey, secretHash, sequence)
console.log('redeemScript  ', redeemScript.toString('hex'))

const p2sh = bitcoin.payments.p2sh({redeem: {output: redeemScript, network: NETWORK}, network: NETWORK})
console.log('P2SH address  ', p2sh.address)




const txb = new bitcoin.TransactionBuilder(NETWORK)
txb.addInput('b7d0f24a1fd927fd452c7f42e036cf0c8903bf62f5196e3522af6088d790736f', 1, sequence)
txb.addOutput(p2pkh.address, 999e5)
const tx = txb.buildIncomplete()

/**
 * Creating unlocking script
 */

  const signatureHash = tx.hashForSignature(0, redeemScript, hashType)

const inputScriptFirstBranch = bitcoin.payments.p2sh({
  redeem: {
    input: bitcoin.script.compile([
      bitcoin.script.signature.encode(keyPairAlice0.sign(signatureHash), hashType),
      bitcoin.opcodes.OP_FALSE,
    ]),
    output: redeemScript
  },
}).input

const inputScriptSecondBranch = bitcoin.payments.p2sh({
  redeem: {
    input: bitcoin.script.compile([
      Buffer.from(secret, 'utf8'),
      bitcoin.script.signature.encode(keyPairBob0.sign(signatureHash), hashType),
      
      bitcoin.opcodes.OP_TRUE
    ]),
    output: redeemScript
  }
}).input

tx.setInputScript(0, inputScriptSecondBranch)

console.log('tx.toHex  ', tx.toHex())
console.log(bitcoin.crypto.sha256(Buffer.from(secret, 'utf8')));
