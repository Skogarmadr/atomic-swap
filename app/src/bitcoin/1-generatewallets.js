/** 
  * 1-generatewallets.js
  * 
  * @fileOverview Generation of a wallet for Alice and Bob, create a json file.
  * @author Luca Srdjenovic luca.srdjenovic@he-arc.ch
  * @version 1.0.0
*/
const bip32 = require('bip32')
const bip39 = require('bip39')
const bitcoin = require('bitcoinjs-lib')
const configFilename = './config.json';
var fs = require("fs");

var argv = require('yargs')
    .usage('Usage: $0')
    .help()
    .argv;

if (argv.config) {
   console.log("Using specified config file: "+argv.config);
   var config = require('./'+argv.config);
} else {
   console.log("Using default config (config.json)");
   var config = require(configFilename);
}


if(config.use_regtest){
    var NETWORK = bitcoin.networks.regtest;
  }else {
    if (config.use_testnet) {
        var NETWORK = bitcoin.networks.testnet;
    } else {
        var NETWORK = bitcoin.networks.bitcoin;
    }
}

function getP2PKHAddress (node, network) {
    return bitcoin.payments.p2pkh({ pubkey: node.publicKey, network }).address
}

function getP2PWKHAddress (node, network) {
    return bitcoin.payments.p2wpkh({ pubkey: node.publicKey, network })
}

function getP2SHAddress (p2wpkh, network) {
    return bitcoin.payments.p2wsh({ redeem: p2wpkh, network }).address
}


const wallets = [
    {alice: config.alice.entropy},
    {bob: config.bob.entropy},
  ];
  
const funding_path = "m/44'/0'/0'/0/0"
var objectJSON = "{\n";

wallets.map((wallet, wallet_index) => {
    var key = Object.keys(wallet).toString(), value = wallet[Object.keys(wallet)];

    console.log(`===================${key}======================`);

    objectJSON += `\t"${key}": [\n`
    console.log(key.toString(), 'entropy ---> ', value);

    // Get mnemonic from entropy
    var mnemonic = bip39.entropyToMnemonic(value);
    console.log(key, "phrase : ",mnemonic);
    
    // Get seed from mnemonic
    var seed = bip39.mnemonicToSeedSync(mnemonic)
    console.log(key, "seed : ", seed.toString("hex"));

    // Get master BIP32 master from seed
    var master = bip32.fromSeed(seed, NETWORK)
    //console.log(key, "master :", master);
    

    // Get private key WIF
    prvWIF = master.toWIF()
    console.log(key, "master WIF : ", prvWIF);

    // Get BIP32 extended private key
    xprv = master.toBase58()
    console.log(key, "xprv ---> ", xprv);

     // Get BIP32 extended public key
    xpub = master.neutered().toBase58()
    console.log(key, "xpub ---> ", xpub);
    

    console.log(`===================${key}'s child======================`);

    //Derive 1 set of addresses
    [...Array(1)].map((value, index) => { 
        
    // Get child node
    var child = master.derivePath(funding_path);
    console.log(key, `'s child ${index}`, child.privateKey.toString('hex'));
    
    // Get child wif private key
    var wif = child.toWIF();
    console.log(key, ' child wif -->  ', wif);
    
    // Get child extended private keys
    var childXprv = child.toBase58();
    console.log(key, 'child xprv -->  ', childXprv);
    
    // Get child EC public key
    var ECPubKey = child.publicKey.toString('hex');
    console.log(key, 'child ECPubKey -->  ', ECPubKey);
    
    // Get child EC public key hash
    var ECPubKeyHash = bitcoin.crypto.hash160(child.publicKey).toString('hex');
    console.log(key, 'child ECPubKeyHash -->  ', ECPubKeyHash);
    
    console.log("=== Addresses ===");
    
    // P2PKH
    //var p2pkh = getP2PKHAddress(child);
    var p2pkh = bitcoin.payments.p2pkh({pubkey: child.publicKey, network: NETWORK}).address
    console.log(key, 'child p2pkh -->  ', p2pkh);
    // P2WPKH
    //var p2wpkh = getP2PWKHAddress(child);  
    var p2wpkh = bitcoin.payments.p2wpkh({pubkey: child.publicKey, network: NETWORK})
    var p2wpkhAddress = p2wpkh.address;
    console.log(key, 'child p2wpkhAddress -->  ', p2wpkhAddress);

    //p2sh
    //var p2sh_p2wpkh = getP2SHAddress(p2wpkh)
    var p2sh_p2wpkh = bitcoin.payments.p2sh({redeem: p2wpkh, NETWORK}).address
    console.log(key, 'child p2sh_p2wpkh -->  ', p2sh_p2wpkh);
    console.log();
    
    
    
    if (index === 0) {
        
        objectJSON += `\t\t{\n  \t\t "wif": "${wif}",\n\t\t "pubKey": "${ECPubKey}",\n\t\t "pubKeyHash": "${ECPubKeyHash}",\n\t\t "p2pkh": "${p2pkh}",\n\t\t "p2sh-p2wpkh": "${p2sh_p2wpkh}",\n\t\t "p2wpkh": "${p2wpkhAddress}"\n\t\t}`
      } else {
        objectJSON += `\t\t{\n  \t\t "wif": "${wif}",\n\t\t "pubKey": "${ECPubKey}",\n\t\t "pubKeyHash": "${ECPubKeyHash}",\n\t\t "p2pkh": "${p2pkh}",\n\t\t "p2sh-p2wpkh": "${p2sh_p2wpkh}",\n\t\t "p2wpkh": "${p2wpkhAddress}"\n\t\t},`
      } 

    wallet_index === 1  ? objectJSON+="\n\t]\n}" : objectJSON+="\n\t],\n"

    });

    /**
     * Write data into config.json
     */
    config[key].phrase = mnemonic;
    config[key].seed = seed.toString("hex");
    config[key].prvWIF = prvWIF;
    config[key].xprv = xprv;
    config[key].xpub = xpub;


    fs.writeFile(configFilename, JSON.stringify(config, null, 4), function (err) {
    if (err) return console.log(err);
    //console.log(JSON.stringify(config));
    console.log(key, 'data written to ', configFilename);
    }); 
        
});


    
/**
 * Write data into a file wallets.json
 */

fs.writeFile("./wallets.json", objectJSON, (err) => {
    if (err) {
        console.error(err);
        return;
    };
    console.log("wallets.json has been created");
});
