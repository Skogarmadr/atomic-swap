/** 
  * 2-fundingaddress.js
  * 
  * @fileOverview Generate P2SH address
  * @author Luca Srdjenovic luca.srdjenovic@he-arc.ch
  * @version 1.0.0
*/


const bip65 = require('bip65'); //Timelock encoding
const bitcoin = require('bitcoinjs-lib');
const { alice, bob } = require('./wallets.json');
const hashType = bitcoin.Transaction.SIGHASH_ALL


var argv = require('yargs')
    .usage('Usage: $0')
    .help()
    .argv;

if (argv.config) {
   console.log("Using specified config file: "+argv.config);
   var config = require('./'+argv.config);
} else {
   console.log("Using default config (config.json)");
   var config = require("./config");
}

if(config.use_regtest){
  var NETWORK = bitcoin.networks.regtest;
}else {
  if (config.use_testnet) {
      var NETWORK = bitcoin.networks.testnet;
  } else {
      var NETWORK = bitcoin.networks.bitcoin;
  }
}


function csvCheckSigOutput(senderPubKey, receiverPubKey, hash, sequence) {
  return bitcoin.script.compile([
    
    bitcoin.opcodes.OP_IF, //claim for buyer
        bitcoin.opcodes.OP_SHA256,
        hash,
        bitcoin.opcodes.OP_EQUALVERIFY,
        receiverPubKey,
        bitcoin.opcodes.OP_CHECKSIG,
    bitcoin.opcodes.OP_ELSE, // Refund for seller, lock the transaction until the locktime has been reached
        bitcoin.script.number.encode(sequence), //check locktime
        bitcoin.opcodes.OP_CHECKSEQUENCEVERIFY, // it verifies, drop time of the stack
        bitcoin.opcodes.OP_DROP, //drop time
        senderPubKey,
        bitcoin.opcodes.OP_CHECKSIG,
    bitcoin.opcodes.OP_ENDIF,
    //bitcoin.opcodes.OP_CHECKSIG,
  ])
};

const secret = "test"
console.log(secret);

const secretHash = bitcoin.crypto.sha256(secret);
console.log('hash : ', secretHash.toString('hex'));

const keyPairAlice0 = bitcoin.ECPair.fromWIF(alice[0].wif, NETWORK)
const p2wpkhAlice0 = bitcoin.payments.p2wpkh({pubkey: keyPairAlice0.publicKey, network:NETWORK})

const keyPairBob0 = bitcoin.ECPair.fromWIF(bob[0].wif, NETWORK)

const sequence = bip65.encode({blocks: 5}) 
console.log('sequence  ', sequence)

//const redeemScript = csvCheckSigOutput(keyPairAlice0, keyPairBob0, secretHash, sequence)
const redeemScript = csvCheckSigOutput(keyPairAlice0.publicKey, keyPairBob0.publicKey, secretHash, sequence)
console.log('redeemScript  ', redeemScript.toString('hex'))

const p2sh = bitcoin.payments.p2sh({redeem: {output: redeemScript}, network: NETWORK})
console.log('P2SH address  ', p2sh.address)


/**
 * Write information to transaction.json
 */

 var object = {
   senderPubKey: keyPairAlice0.publicKey.toString('hex'),
   receiverPubKey: keyPairBob0.publicKey.toString('hex'),
   secret: secret,
   secretHash: secretHash.toString('hex'),
   sequence: sequence,
   p2shAddress: p2sh.address,
   txid: "",
   vout: ""
 }

var fs = require("fs");

fs.writeFile("./transaction.json", JSON.stringify(object, null, 4), function (err) {
  if (err) return console.log(err);
  //console.log(JSON.stringify(config));
  console.log("Transaction file has been written.");
  }); 

//sendtoaddress [p2sh.address] 1

//gettransaction "txid"


