const config = require("./config");
const request = require('request')
const bitcoin = require('bitcoinjs-lib')
const bip68 = require('bip68');
const fs = require('fs')
const { alice, bob } = require('./wallets.json');
const hashType = bitcoin.Transaction.SIGHASH_ALL
const APINETWORK = 'BTCTEST/'



function getNetwork() {
  if (config.use_testnet) {
    var NETWORK = bitcoin.networks.testnet
  } else {
    var NETWORK = bitcoin.networks.bitcoin;
  }

  return NETWORK;
}

function csvCheckSigOutput(senderPubkeyHash, receiverPubkeyHash, hash, sequence) {
  return bitcoin.script.compile([
    bitcoin.opcodes.OP_IF, //claim for buyer
    bitcoin.opcodes.OP_SHA256,
    hash,
    bitcoin.opcodes.OP_EQUALVERIFY,
    bitcoin.opcodes.OP_DUP,
    bitcoin.opcodes.OP_HASH160,
    receiverPubkeyHash,
    bitcoin.opcodes.OP_ELSE, // Refund for seller, lock the transaction until the locktime has been reached
    bitcoin.script.number.encode(sequence), //check locktime
    bitcoin.opcodes.OP_CHECKSEQUENCEVERIFY, // it verifies, drop time of the stack
    bitcoin.opcodes.OP_DROP, //drop time
    bitcoin.opcodes.OP_DUP,
    bitcoin.opcodes.OP_HASH160,
    senderPubkeyHash,
    bitcoin.opcodes.OP_ENDIF,
    bitcoin.opcodes.OP_EQUALVERIFY,
    bitcoin.opcodes.OP_CHECKSIG,
  ]);
};

function main() {

  const NETWORK = getNetwork();

  const revSecret1 = 'test'
  const revSecret2 = Buffer.from(revSecret1);
  console.log(revSecret2);

  const revHash = bitcoin.crypto.sha256(revSecret2);

  console.log('revhash : ' , revHash.toString('hex'));

  const keyPairAlice = bitcoin.ECPair.fromWIF(alice[0].wif, NETWORK)
  const addressAlice = bitcoin.payments.p2pkh({ pubkey: keyPairAlice.publicKey, network: NETWORK }).address
  const pubKeyAliceHash = bitcoin.crypto.hash160(keyPairAlice.publicKey);
  console.log('Alice pubkey hash : ' + pubKeyAliceHash.toString('hex'));


  console.log(addressAlice);

  const keyPairBob = bitcoin.ECPair.fromWIF(bob[0].wif, NETWORK);
  const pubKeyBobHash = bitcoin.crypto.hash160(keyPairBob.publicKey);
  console.log('Bob pubkey hash : ' + pubKeyBobHash.toString('hex'));

  // Encode the sequence value according to BIP68 specification (now + 2 blocks).
  const sequence = bip68.encode({ blocks: 2 })
  console.log('sequence  ', sequence) 

  const redeemScript = csvCheckSigOutput(pubKeyAliceHash, pubKeyBobHash, revHash, sequence)
  console.log('redeemScript  ', redeemScript.toString('hex'))

  const p2sh = bitcoin.payments.p2sh({ redeem: { output: redeemScript }, network: NETWORK })
  console.log('P2SH address  ', p2sh.address)


  //fund(NETWORK, keyPairAlice, p2sh.address, redeemScript, 290711);

  //spendRefund(NETWORK, keyPairAlice, redeemScript, p2sh.address, sequence);
  revokeFund(NETWORK, keyPairBob, redeemScript, p2sh.address, sequence, 'test')
}

function fund(NETWORK, keyPairSender, p2shAddress, redeemScript, amount) {
  const addressSender = bitcoin.payments.p2pkh({ pubkey: keyPairSender.publicKey, network: NETWORK }).address
  
  request('https://chain.so/api/v2/get_tx_unspent/' + APINETWORK + addressSender, function (error, response, body) {
    if (!error && response.statusCode == 200) {

      var info = JSON.parse(body)
      var utxos = info.data.txs;

      if (utxos.length == 0) {
        console.log("no utxos found in funding address : ", addressSender);
      } else {

        let txb = new bitcoin.TransactionBuilder(NETWORK);

        let totalBalance = 0;
        let input_count = 0;

        for (var utxo in utxos) {

          totalBalance += parseFloat(utxos[utxo].value);
          let unspent = utxos[utxo];
          txb.addInput(unspent.txid, unspent.output_no)
          console.log("adding input: " + unspent.txid + " to funding transaction")
          input_count++;
        }

        console.log(`Balance : ${totalBalance} btc`);

        request('https://insight.bitpay.com/api/utils/estimatefee?nbBlocks=4', function (error, response, body) {
          var estimatefee = JSON.parse(body)
          if (estimatefee["4"] > 0) {
            var estimated_tx_size = input_count * 148 + 32 + 1;

            var estimated_fee_amount = estimatefee["4"] * (estimated_tx_size / 1024)
            log
            var total_amount = Math.floor((totalBalance - estimated_fee_amount) * 100000000)
            console.log(`Amount sendable : ${total_amount} satoshis`);

            if (total_amount - amount >= 0) {
              if(total_amount - amount != 0)
              {
                let remain = total_amount - amount;
                txb.addOutput(addressSender, remain);
              }
             
              txb.addOutput(p2shAddress, amount);
              

              for (var i = 0; i < input_count; i++) {
                txb.sign(i, keyPairSender)
              }

              var funding_transaction = txb.buildIncomplete()
              console.log("")
              console.log("===== Funding Transaction to " + p2shAddress + " ) =======")
              console.log("Transaction ID: " + funding_transaction.getId())
              console.log("Transaction (needed for broadcasting TX): " + funding_transaction.toHex())
              //console.log("Funding tx size: "+funding_transaction.byteLength())
              console.log("")

              broadcastTx(funding_transaction.toHex());

            } else {
              console.log("You don't have enough Satoshis to cover the miner fee !");
            }
          }
        });
      }
    }

  });
}

function broadcastTx(txHex) {


  request.post({ url: 'https://chain.so/api/v2/send_tx/' + APINETWORK, body: "tx_hex=" + txHex }, function (err, httpResponse, body) {
    if (!err && httpResponse.statusCode == 200) {
      console.log("Transaction has been sent !");
    } else {
      console.log("Transaction has not been sent !");
      console.log(body);
    }
  });

}

function spendRefund(NETWORK, keyPairReceiver, redeemScript, p2shAddress, sequence) {

  request('https://chain.so/api/v2/get_tx_unspent/' + APINETWORK + p2shAddress, function (error, response, body) {
    if (!error && response.statusCode == 200) {

      var info = JSON.parse(body)
      var utxos = info.data.txs;

      if (utxos.length == 0) {
        console.log("no utxos found in funding address : ", p2shAddress);
      } else {

        let txb = new bitcoin.TransactionBuilder(NETWORK);
        txb.setVersion(2);

        let totalBalance = 0;
        let input_count = 0;

        for (var utxo in utxos) {
          if(input_count == 1)
          {

          } else 
          {
            totalBalance += parseFloat(utxos[utxo].value);
            let unspent = utxos[utxo];
            txb.addInput(unspent.txid, unspent.output_no, sequence)
            console.log("adding input: " + unspent.txid + " to refunding transaction")
            input_count++;
          }
         
        }

        console.log(`Balance of p2sh : ${totalBalance} btc`);

        request('https://insight.bitpay.com/api/utils/estimatefee?nbBlocks=4', function (error, response, body) {
          var estimatefee = JSON.parse(body)
          if (estimatefee["4"] > 0) {
            var estimated_tx_size = input_count * 148 + 32 + 1;

            var estimated_fee_amount = estimatefee["4"] * (estimated_tx_size / 1024)
            console.log(estimated_fee_amount);

            var send_amount = Math.floor((totalBalance - estimated_fee_amount) * 100000000)
            console.log(`Amount sendable : ${send_amount} satoshis`);

            const senderAddress = bitcoin.payments.p2pkh({ pubkey: keyPairReceiver.publicKey, network: NETWORK }).address
            txb.addOutput(senderAddress, send_amount);

            txRaw = txb.buildIncomplete();

            

            const signatureHash = txRaw.hashForSignature(0, redeemScript, hashType)

            var redeemScriptSig = bitcoin.payments.p2sh({
              redeem: {
                input: bitcoin.script.compile([
                  bitcoin.script.signature.encode(keyPairReceiver.sign(signatureHash), hashType),
                  keyPairReceiver.publicKey,
                  bitcoin.opcodes.OP_FALSE
                ]),
                output: redeemScript
              },
            }).input  
    
            txRaw.setInputScript(0, redeemScriptSig)
    
            console.log("")
            console.log("===== REFUND TX from "+ p2shAddress+" to Alices address =======")
            console.log("attempting to broadcast transaction:")
            console.log("Transaction ID: "+txRaw.getId())
            console.log("Transaction: "+txRaw.toHex())

            broadcastTx(txRaw.toHex());
          }
        }); 

      }

    }
  });


}

function revokeFund(NETWORK, keyPairReceiver, redeemScript, p2shAddress, sequence, preimage) {

  request('https://chain.so/api/v2/get_tx_unspent/' + APINETWORK + p2shAddress, function (error, response, body) {
    if (!error && response.statusCode == 200) {

      var info = JSON.parse(body)
      var utxos = info.data.txs;

      if (utxos.length == 0) {
        console.log("no utxos found in funding address : ", p2shAddress);
      } else {

        let txb = new bitcoin.TransactionBuilder(NETWORK);
        txb.setVersion(2);

        let totalBalance = 0;
        let input_count = 0;

        for (var utxo in utxos) {

          totalBalance += parseFloat(utxos[utxo].value);
          let unspent = utxos[utxo];
          txb.addInput(unspent.txid, unspent.output_no, 0)
          console.log("adding input: " + unspent.txid + " to refunding transaction")
          input_count++;
        }

        console.log(`Balance of p2sh : ${totalBalance} btc`);

        request('https://insight.bitpay.com/api/utils/estimatefee?nbBlocks=4', function (error, response, body) {
          var estimatefee = JSON.parse(body)
          if (estimatefee["4"] > 0) {
            var estimated_tx_size = input_count * 148 + 32 + 1;

            var estimated_fee_amount = estimatefee["4"] * (estimated_tx_size / 1024)
            console.log(estimated_fee_amount);

            var send_amount = Math.floor((totalBalance - estimated_fee_amount) * 100000000)
            console.log(`Amount sendable : ${send_amount} satoshis`);

            const senderAddress = bitcoin.payments.p2pkh({ pubkey: keyPairReceiver.publicKey, network: NETWORK }).address

            console.log(senderAddress);
            
            txb.addOutput(senderAddress, send_amount);

            txRaw = txb.buildIncomplete();
           
            const signatureHash = txRaw.hashForSignature(0, redeemScript, hashType)

            var redeemScriptSig = bitcoin.payments.p2sh({
              redeem: {
                input: bitcoin.script.compile([
                  bitcoin.script.signature.encode(keyPairReceiver.sign(signatureHash), hashType),
                  keyPairReceiver.publicKey,
                  Buffer.from(preimage),
                  bitcoin.opcodes.OP_TRUE
                ]),
                output: redeemScript
              },
            }).input

            txRaw.setInputScript(0, redeemScriptSig);
    
            console.log("")
            console.log("===== Claim TX from "+ p2shAddress+" to Bob address =======")
            console.log("attempting to broadcast transaction:")
            console.log("Transaction ID: "+txRaw.getId())
            console.log("Transaction: "+txRaw.toHex())

            broadcastTx(txRaw.toHex());
          }
        }); 

      }

    }
  });
}

async function getUnspentUtxos(address) {
  request('https://chain.so/api/v2/get_tx_unspent/' + APINETWORK + address, function (error, response, body) {
    if (!error && response.statusCode == 200) {

      var info = JSON.parse(body)
      var utxos = info.data.txs;

      return utxos;
    }
  });
}



function getBalance(address) {
  request('https://chain.so/api/v2/get_address_balance/' + APINETWORK + address, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body)

      balance = info.data.confirmed_balance;

      return balance;
    }

  });
}

function getBlockHeight() {
  request('https://chain.so/api/v2/get_info/' + APINETWORK, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body)

      height = info.data.blocks;

      console.log('block heigt :' + height);


    } else {
      console.log(error);

    }

  });
}




main();

