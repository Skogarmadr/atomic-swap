2  /** 
  * 5-bobrevokefund.js
  * 
  * @fileOverview Fund P2SH address
  * @author Luca Srdjenovic luca.srdjenovic@he-arc.ch
  * @version 1.0.0
*/


const bip65 = require('bip65'); //Timelock encoding
const bitcoin = require('bitcoinjs-lib');
const { alice, bob } = require('./wallets.json');
var request = require('request');
const explorers = require('bitcore-explorers')



var argv = require('yargs')
    .usage('Usage: $0 <redeemScript>')
    .help()
    .argv;


if (argv.config) {
   console.log("Using specified config file: "+argv.config);
   var config = require('./'+argv.config);
} else {
   console.log("Using default config (config.json)");
   var config = require("./config");
}

if(argv._.length != 1){
  console.log("please call this script with redeemScript")
  console.log('Usage: 5-bobrevokefund.js <redeemScript>')

}else{
  if(config.use_regtest){
    var NETWORK = bitcoin.networks.regtest;
  }else {
    if (config.use_testnet) {
      var NETWORK = bitcoin.networks.testnet
      var client = new explorers.Insight("testnet");
    } else {
      var NETWORK = bitcoin.networks.bitcoin;
      var client = new explorers.Insight();
    }
  }
}
const hashType = bitcoin.Transaction.SIGHASH_ALL

const secret = "test"

var redeemScript = Buffer.from(argv._[0], 'hex')

var p2shAddress = '2MzHp5GiF1QykVisHKqSW8dwjJXTFkb76b2'


const keyPairBob = bitcoin.ECPair.fromWIF(bob[0].wif, NETWORK)
const bobAddress = bitcoin.payments.p2pkh({pubkey: keyPairBob.publicKey, network:NETWORK}).address

client.getUnspentUtxos(p2shAddress, function(err, utxos) {
  if(utxos.length == 0){
      console.log("no utxos found in redeemScript address ("+p2shAddress+") !")
  }else{

    console.log("create transaction to spend from "+redeemScript.toString('hex'))
    console.log("with secret for revhash: "+secret.toString('hex'))



    var tx = new bitcoin.TransactionBuilder(NETWORK)
    tx.setVersion(2)

    var total = 0;
    var input_count = 0;

    for (var utxo in utxos) {
      total += utxos[utxo].satoshis
      var res = utxos[utxo].toString().split(":")
      tx.addInput(res[0], parseInt(res[1]), 2)
      input_count++
    }

    console.log(' total satoshis : ' + total );
    
    request('https://insight.bitpay.com/api/utils/estimatefee?nbBlocks=4', function (error, response, body) {

      var estimatefee = JSON.parse(body)

      if(estimatefee[4] > 0)
       {
        estimated_tx_size = input_count*148 + 32 + 10
        p2sh_estimated_tx_size = 600
        // console.log("estimated tx size: "+estimated_tx_size)
        estimated_fee_amount = (estimatefee["4"]*100000000)*(p2sh_estimated_tx_size/1024)
        console.log("estimated fee amount: "+estimated_fee_amount)
        var send_amount = total - Math.floor(estimated_fee_amount)
        console.log("send amount: "+send_amount)
        tx.addOutput(bobAddress, send_amount)
  
        txRaw = tx.buildIncomplete();

        /**
         * Creating unlocking script
         */ 

        const signatureHash = txRaw.hashForSignature(0, redeemScript, hashType)

        var redeemScriptSig = bitcoin.payments.p2sh({
          redeem: {
            input: bitcoin.script.compile([
              bitcoin.script.signature.encode(keyPairBob.sign(signatureHash), hashType),
              Buffer.from(secret, 'utf8'),
              bitcoin.opcodes.OP_TRUE
            ]),
            output: redeemScript
          }
        }).input

        txRaw.setInputScript(0, redeemScriptSig)

        console.log("")
        console.log("===== REFUND TX from "+p2shAddress+" to Bob address " + bobAddress + " =======")
        console.log("attempting to broadcast transaction:")
        console.log("Transaction ID: "+txRaw.getId())
        console.log("Transaction: "+txRaw.toHex())

        
        client.broadcast(txRaw.toHex(), function(err, result) {
          if(!err){
              console.log(result)

              console.log("Transaction broadcasted ")
          }else{
              console.log(err)
          }
      })
      
        
       }
      
    });

  }

});