 /** 
  * 2-fundingandrefund.js
  * 
  * @fileOverview Fund P2SH address
  * @author Luca Srdjenovic luca.srdjenovic@he-arc.ch
  * @version 1.0.0
*/


const bip65 = require('bip65'); //Timelock encoding
const bitcoin = require('bitcoinjs-lib');
const { alice, bob } = require('./wallets.json');
var request = require('request');
const explorers = require('bitcore-explorers')



var argv = require('yargs')
    .usage('Usage: $0')
    .help()
    .argv;

if (argv.config) {
   console.log("Using specified config file: "+argv.config);
   var config = require('./'+argv.config);
} else {
   console.log("Using default config (config.json)");
   var config = require("./config");
}

if(argv._.length != 1){
  console.log("please call this script with fully signed refund transaction & funding transaction.")
  console.log('Usage: 4-verifyrefundandbroadcastfunding.js <fundingtx>')
}else{
  if(config.use_regtest){
    var NETWORK = bitcoin.networks.regtest;
  }else {
    if (config.use_testnet) {
  
      
      var NETWORK = bitcoin.networks.testnet
      var client = new explorers.Insight("testnet");
  
  
    } else {
      var NETWORK = bitcoin.networks.bitcoin;
      var client = new explorers.Insight();
    }
  }
}


var funding_tx  = bitcoin.Transaction.fromHex(argv._[0]);

console.log(funding_tx)

client.broadcast(argv._[0], function(err, result) {
  if(!err){
      console.log(result)
  }else{
      console.log(err)
  }
})

