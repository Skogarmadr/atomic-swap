/** 
  * 2-fundingandrefund.js
  * 
  * @fileOverview Fund P2SH address
  * @author Luca Srdjenovic luca.srdjenovic@he-arc.ch
  * @version 1.0.0
*/


const bip65 = require('bip65'); //Timelock encoding
const bitcoin = require('bitcoinjs-lib');
const { alice, bob } = require('./wallets.json');
var request = require('request');
const explorers = require('bitcore-explorers')



var argv = require('yargs')
    .usage('Usage: $0')
    .help()
    .argv;

if (argv.config) {
   console.log("Using specified config file: "+argv.config);
   var config = require('./'+argv.config);
} else {
   console.log("Using default config (config.json)");
   var config = require("./config");
}

if(config.use_regtest){
  var NETWORK = bitcoin.networks.regtest;
}else {
  if (config.use_testnet) {

    
    var NETWORK = bitcoin.networks.testnet
    var client = new explorers.Insight("testnet");


  } else {
    var NETWORK = bitcoin.networks.bitcoin;
    var client = new explorers.Insight();
  }
}




function csvCheckSigOutput(senderPubkey, receiverPubkey, hash, sequence) {
  return bitcoin.script.compile([ 
    bitcoin.opcodes.OP_IF, //claim for buyer
        bitcoin.opcodes.OP_SHA256,
        hash,
        bitcoin.opcodes.OP_EQUALVERIFY,
        receiverPubkey,
        bitcoin.opcodes.OP_CHECKSIG,
    bitcoin.opcodes.OP_ELSE, // Refund for seller, lock the transaction until the locktime has been reached
        bitcoin.script.number.encode(sequence), //check locktime
        bitcoin.opcodes.OP_CHECKSEQUENCEVERIFY, // it verifies, drop time of the stack
        bitcoin.opcodes.OP_DROP, //drop time
        senderPubkey,
        bitcoin.opcodes.OP_CHECKSIG,
    bitcoin.opcodes.OP_ENDIF,
    //bitcoin.opcodes.OP_CHECKSIG,
  ])
};

const secret = "test"
console.log(secret);

const secretHash = bitcoin.crypto.sha256(secret);
console.log('hash : ', secretHash.toString('hex'));

const keyPairAlice = bitcoin.ECPair.fromWIF(alice[0].wif, NETWORK)
const addressAlice = bitcoin.payments.p2pkh({pubkey: keyPairAlice.publicKey, network:NETWORK}).address

console.log(addressAlice);

const keyPairBob = bitcoin.ECPair.fromWIF(bob[0].wif, NETWORK)

const sequence = bip65.encode({blocks: 2}) 
console.log('sequence  ', sequence)

//const redeemScript = csvCheckSigOutput(n  , keyPairBob0, secretHash, sequence)
const redeemScript = csvCheckSigOutput(keyPairAlice.publicKey, keyPairBob.publicKey, secretHash, sequence)
console.log('redeemScript  ', redeemScript.toString('hex'))

const p2sh = bitcoin.payments.p2sh({redeem: {output: redeemScript}, network: NETWORK})
console.log('P2SH address  ', p2sh.address)





client.getUnspentUtxos(addressAlice, function(err, utxos) {
    if(utxos.length == 0){
        console.log("no utxos found in funding address!")
    }else{

        var tx = new bitcoin.TransactionBuilder(NETWORK)
      
        var total = 0;
        var input_count = 0;
        for (var utxo in utxos) {
            total += utxos[utxo].satoshis
            var res = utxos[utxo].toString().split(":")
            console.log("adding input: "+res[0]+" to funding transaction")
            console.log(parseInt(res[1]));
            
            tx.addInput(res[0], parseInt(res[1]))
            input_count++

        }

        request('https://insight.bitpay.com/api/utils/estimatefee?nbBlocks=4', function (error, response, body) {
            var estimatefee = JSON.parse(body)
            console.log(estimatefee);
            if(estimatefee["4"] > 0){
              estimated_tx_size = input_count*148 + 32 + 10
              estimated_fee_amount = (estimatefee["4"]*100000000)*(estimated_tx_size/1024)
              var send_amount = total - Math.floor(estimated_fee_amount)
              console.log(send_amount);
              

              tx.addOutput(p2sh.address, send_amount)
            // txb.addOutput(addressAlice, 1e4) // Alice's change

            for(var i = 0; i < input_count; i++)
            {
              tx.sign(i, keyPairAlice)
            }

              var funding_transaction = tx.buildIncomplete()
              console.log("")
              console.log("===== Funding Transaction to "+p2sh.address+" ( needed for 4-verifyrefundandbroadcastfunding.js) =======")
              console.log("Transaction ID: "+funding_transaction.getId())
              console.log("Transaction (needed for 4-verifyrefundandbroadcastfunding.js): "+funding_transaction.toHex())
              //console.log("Funding tx size: "+funding_transaction.byteLength())
              console.log("")

              //make the refund revpubkey address
              

            console.log('redeemScript : ', p2sh.redeem.output.toString('hex'));
            //const signatureHash = tx.hashForSignature(0, redeemScript, hashType)

            
            console.log("")
            console.log("===== REDEEM SCRIPT for "+p2sh.address+" (address of p2sh; needed for 7-spendrefund.js) =======")
            console.log("script: "+p2sh.redeem.output.toString('hex'))
            console.log("script hash: "+bitcoin.crypto.sha256(p2sh.redeem.output).toString('hex'))
            console.log("rev hash (share with bob): "+secretHash.toString('hex'))
            console.log("rev secret (shared with bob to provide finality of next channel transaction): "+secret.toString())

          

            }

        })
    }

});
